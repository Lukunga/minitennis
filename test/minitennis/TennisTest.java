/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minitennis;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JLukunga
 */
public class TennisTest {
    
    public TennisTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of firstPlayerScore method, of class Tennis.
     */
    @Test
    public void testFirstPlayerScore() {
        System.out.println("testFirstPlayerScore");
        Tennis instance = new Tennis("Federer", "Nadal");
        int before = instance.getScorePlayer1();
        instance.firstPlayerScore();
        int after = instance.getScorePlayer1();
        // TODO review the generated test code and remove the default call to fail.
        if(before == after){
            fail("testFirstPlayerScore() failed !");
        }else{
            //fail("testFirstPlayerScore() succed !");
            System.out.println("Succes of testFristPlayerScore because before = " + before + " and after = " + after);
        }
    }

    /**
     * Test of secondPlayerScore method, of class Tennis.
     */
    @Test
    public void testSecondPlayerScore() {
        System.out.println("testSecondPlayerScore");
        Tennis instance = new Tennis("Federer", "Nadal");
        int before = instance.getScorePlayer2();
        instance.secondPlayerScore();
        int after = instance.getScorePlayer2();
        // TODO review the generated test code and remove the default call to fail.
        if(before == after){
            fail("testSecondPlayerScore() failed !");
        }else{
            //fail("testFirstPlayerScore() succed !");
            System.out.println("Succes of testSecondPlayerScore because before = " + before + " and after = " + after);
        }
    }

    /**
     * Test of deuce method, of class Tennis.
     */
    @Test
    public void testDeuce() {
        System.out.println("testDeuce");
        Tennis instance = new Tennis("Federer", "Nadal");
        
        instance.setScorePlayer1(4);
        instance.setScorePlayer2(4);
        
        boolean expResult = true;
        boolean result = instance.deuce();
        assertEquals(expResult, result);
    }

    /**
     * Test of advantage method, of class Tennis.
     */
    @Test
    public void testAdvantage() {
        System.out.println("testAdvantage");
        Tennis instance = new Tennis("Federer", "Nadal");
        
        instance.setScorePlayer1(8);
        instance.setScorePlayer2(7);
        
        boolean expResult = true;
        boolean result = instance.advantage();
        assertEquals(expResult, result);
    }

    /**
     * Test of playerWon method, of class Tennis.
     */
    @Test
    public void testPlayerWon() {
        System.out.println("testPlayerWon");
        Tennis instance = new Tennis("Federer", "Nadal");
        
        instance.setScorePlayer1(5);
        instance.setScorePlayer2(3);
        
        boolean expResult = true;
        boolean result = instance.playerWon();
        assertEquals(expResult, result);
    }

    /**
     * Test of playerWithHigherScore method, of class Tennis.
     */
    @Test
    public void testPlayerWithHigherScore() {
        System.out.println("testPlayerWithHigherScore");
        Tennis instance = new Tennis("Federer", "Nadal");
        
        instance.setScorePlayer1(5);
        instance.setScorePlayer2(3);
        
        String expResult = "Federer";
        String result = instance.playerWithHigherScore();
        assertEquals(expResult, result);
    }

    /**
     * Test of convertScore method, of class Tennis.
     */
    @Test
    public void testConvertScore() {
        System.out.println("testConvertScore");
        Tennis instance = new Tennis("Federer", "Nadal");
        
        instance.setScorePlayer1(2);
        int score = instance.getScorePlayer1();
        
        String expResult = "thirty";
        String result = instance.convertScore(score);
        assertEquals(expResult, result);
    }

    /**
     * Test of manageGame method, of class Tennis.
     */
    @Test
    public void testManageGame() {
        System.out.println("testManageGame");
        Tennis instance = new Tennis("Federer", "Nadal");
        
        instance.setScorePlayer1(4);
        instance.setScorePlayer2(4);
        
        String expResult = "Deuce";
        String result = instance.manageGame();
        assertEquals(expResult, result);
    }
    
}
